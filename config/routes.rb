ToyRails::Application.routes.draw do
  resources :toys
  root :to => 'toys#index'
end
